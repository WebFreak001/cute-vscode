# Cute Theme

A pink light color theme for VSCode.

Code area is mostly like standard Light+ theme, a lot of the other areas are adjusted.

Preview:

![preview.png](preview.png)
